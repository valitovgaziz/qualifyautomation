package ru.bellintegrator.pageObjects.gladiolus;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * author:  ValitovGaziz
 * date:    08.04.2022
 * project: Task1.1
 */

public class AfterSearchGladiolus extends BeforeSearchGladiolus {

    private final WebDriverWait webDriverWait;

    WebElement result;

    public AfterSearchGladiolus(WebDriver webDriver) {
        super(webDriver);
        this.webDriverWait = new WebDriverWait(webDriver, 5);
    }

    public WebElement isContainWikiLink(String url) {
        String xPath = "//a[contains(@href, '" + url + "')]";
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        result = webDriver.findElement(By.xpath(xPath));
        webDriverWait.until(ExpectedConditions.visibilityOf(result));
        return result;
    }

}
