package ru.bellintegrator.pageObjects.gladiolus;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * author:  ValitovGaziz
 * date:    06.04.2022
 * project: Task1.1
 */

public class BeforeSearchGladiolus {

    final WebDriver webDriver;


    WebElement input;


    public BeforeSearchGladiolus(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.input = webDriver.findElement(By.xpath("//input"));

    }

    public void find(String word) {
        input.click();
        input.sendKeys(word);
        input.submit();
    }



}
