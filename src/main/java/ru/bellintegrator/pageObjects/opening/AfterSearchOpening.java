package ru.bellintegrator.pageObjects.opening;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * author:  ValitovGaziz
 * date:    08.04.2022
 * project: Task1.1
 */


public class AfterSearchOpening extends BeforeSearchOpening {
    List<WebElement> results; // Результаты поиска в ul List<WebElement>.
    WebDriverWait webDriverWait;
    List<WebElement> rowWithCourses; // Строки с курсами валют

    public AfterSearchOpening(WebDriver webDriver) {
        super(webDriver);
        this.webDriverWait = new WebDriverWait(webDriver, 12);
    }

    public List<WebElement> getResults() {
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@aria-label='Результаты поиска']")));
        results = webDriver.findElements(By.xpath("//ul[@aria-label='Результаты поиска']//li"));
        return results;
    }

    public boolean openLink(String blockOpening) {
        results.forEach(webElement -> {
            if (webElement.getText().contains(blockOpening)) {
                int some = 1;
                webElement.findElement(By.xpath("//a[@href='https://www.Open.ru/']")).click();
            }
        });
        webDriverWait.until(ExpectedConditions.numberOfWindowsToBe(2)); // Ожидание открытия второй вкладки (окна) после перехода по ссылке
        List<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
        webDriver.switchTo().window(tabs.get(1)); // Переход на вкладку "Открытие банка"
        return true;
    }

    public boolean getExchangeRateElements() {
        webDriverWait.until( // Ожидание загрузки элемента
                ExpectedConditions
                        .visibilityOfAllElementsLocatedBy
                                (By.xpath("//table[contains(@class, 'main-page-exchange__table')]//tbody//tr"))
        );
        rowWithCourses = // Получение строк таблицы с курсами валют
                webDriver.findElements(By.xpath("//table[contains(@class, 'main-page-exchange__table')]//tbody//tr"));
        if (rowWithCourses.size() < 1) {
            return false;
        }
        // Проверка на существование курса обменов валют USD и EUR
        return rowWithCourses.stream().anyMatch(webElement -> webElement.getText().contains("USD")) &&
                rowWithCourses.stream().anyMatch(webElement -> webElement.getText().contains("EUR"));
    }

    // Проверка на то что курс продажи больще курса покупки у USD И EUR
    public boolean checkRates() {
        int indexBay = -1;
        int indexSell = -1;
        rowWithCourses =
                webDriver.findElements(By.xpath("//table[contains(@class, 'main-page-exchange__table')]//tbody//tr"));
        List<WebElement> header = rowWithCourses.get(0).findElements(By.xpath("td[@class='main-page-exchange__td']"));
        for (int i = 0; i < header.size(); i++) {
            if (header.get(i).getText().contains("Банк покупает")) indexBay = i;
            else if (header.get(i).getText().contains("Банк продает")) indexSell = i;
        }
        if (indexBay < 0 || indexSell < 0) return false;
        List<WebElement> rowUSD = new ArrayList<>();
        rowWithCourses.forEach(webElement -> {
            if (webElement.getText().contains("USD"))
                rowUSD.addAll(webElement.findElements(By.xpath(".//span")));
        });
        int check = 0;
        double sellRateUSD = Double.parseDouble(rowUSD.get(indexSell).getText().replace(",", "."));
        double byRateUSD = Double.parseDouble(rowUSD.get(indexBay).getText().replace(",",  "."));
        List<WebElement> rowEUR = new ArrayList<>();
        rowWithCourses.forEach(element -> {
            if (element.getText().contains("EUR"))
                rowEUR.addAll(element.findElements(By.xpath(".//span")));
        });
        double sellRateEUR = Double.parseDouble(rowEUR.get(indexSell).getText().replace(",", "."));
        double byRateEUR = Double.parseDouble(rowEUR.get(indexBay).getText().replace(",", "."));

        return byRateUSD < sellRateUSD && byRateEUR < sellRateEUR;
    }

}
