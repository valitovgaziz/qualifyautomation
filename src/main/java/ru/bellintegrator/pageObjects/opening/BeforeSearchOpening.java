package ru.bellintegrator.pageObjects.opening;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * author:  ValitovGaziz
 * date:    08.04.2022
 * project: Task1.1
 */

public class BeforeSearchOpening {


    final WebDriver webDriver;


    WebElement input;


    public BeforeSearchOpening(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.input = webDriver.findElement(By.xpath("//input[@class]"));

    }

    public void find(String word) {
        input.click();
        input.sendKeys(word);
        input.submit();
    }

}
