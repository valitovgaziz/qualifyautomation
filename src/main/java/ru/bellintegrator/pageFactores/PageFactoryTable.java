package ru.bellintegrator.pageFactores;


import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.List;

/**
 * author:  ValitovGaziz
 * date:    07.04.2022
 * project: Task1.1
 */

@Data
public class PageFactoryTable {

    private WebDriver webDriver;

    public PageFactoryTable(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private int index = 0;

    @FindBy(how = How.XPATH, using = "//input[@class]")
    WebElement input;


    @FindBy(how = How.XPATH, using = "//table")
    List<WebElement> elements;

    // Результаты поиска
    @FindBy(how = How.XPATH, using = "//ul[@aria-label='Результаты поиска']//li")
    List<WebElement> results;

    public void find(String word) {
        input.click();
        input.sendKeys(word);
        input.submit();
    }


    public boolean clickAndSwitchToWikiPage() {

        results.forEach(webElement -> {
            if (webElement.getText().contains("Таблица — Википедия")) {
                webElement.findElement(By.tagName("a")).click();
            }
        });

        List<String> tabs = new ArrayList<String>(webDriver.getWindowHandles());
        if (tabs.size() > 1) {
            webDriver.switchTo().window(tabs.get(1));
            return true;
        }
        return false;
    }

    public boolean sureThatTableCaption() {
        for (int i = 0; i < elements.size(); i++) {
            WebElement next = elements.get(i).findElement(By.tagName("caption"));
            if (next.getText().contains("Преподаватели кафедры программирования")) {
                index = i;
                return true;
            }
        }

        return false;
    }

    public boolean sureFirstTeacher(String firstTeacher) {
        String name = firstTeacher.split(" ")[0];
        String lastName = firstTeacher.split(" ")[1];
        WebElement table = elements.get(index);
        List<WebElement> webElements = table.findElements(By.tagName("tr"));
        List<WebElement> first = webElements.get(1).findElements(By.tagName("td"));
        return first.get(1).getText().contains(name) && first.get(2).getText().contains(lastName);
    }

    public boolean sureLastTeacher(String lastTeacher) {
        String name = lastTeacher.split(" ")[0];
        String lastName = lastTeacher.split(" ")[1];
        WebElement table = elements.get(index);
        List<WebElement> webElements = table.findElements(By.tagName("tr"));
        List<WebElement> first = webElements.get(webElements.size() - 1).findElements(By.tagName("td"));
        return first.get(1).getText().contains(name) && first.get(2).getText().contains(lastName);
    }
}
