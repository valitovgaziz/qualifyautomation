package ru.bellintegrator;


import io.qameta.allure.Feature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.support.PageFactory;
import ru.bellintegrator.pageFactores.PageFactoryTable;
import ru.bellintegrator.pageObjects.gladiolus.AfterSearchGladiolus;
import ru.bellintegrator.pageObjects.gladiolus.BeforeSearchGladiolus;
import ru.bellintegrator.pageObjects.opening.AfterSearchOpening;
import ru.bellintegrator.pageObjects.opening.BeforeSearchOpening;

/**
 * author:  ValitovGaziz
 * date:    06.04.2022
 * project: Task1.1
 */

public class FirstTask extends BaseTest {

    @Feature("Проверка результатов поиска")
    @DisplayName("Проверка результатов поиска с помощью PO")
    @ParameterizedTest(name = "{displayName}: {arguments}")
    @CsvSource({"https://www.google.com, Гладиолус, 'https://ru.wikipedia.org'"})
    public void isContainWikiLing(String pageURL, String gladiolus, String url) {
        chromeDriver.get(pageURL);
        BeforeSearchGladiolus poGladiolusBeforeSearch = new BeforeSearchGladiolus(chromeDriver);
        poGladiolusBeforeSearch.find(gladiolus);
        AfterSearchGladiolus afterSearchGladiolus = new AfterSearchGladiolus(chromeDriver);
        Assertions.assertNotNull(
                afterSearchGladiolus.isContainWikiLink(url),
                "После ввода в поле 'input' строку 'Гладиолус' " +
                        "и получения результата" +
                        "на первой странице нет ссылки содержащей путь '" + gladiolus + "'"
        );
    }

    @Feature("Проверка результатов поиска и курсов валют")
    @DisplayName("Проверка результатов поиска с помощью PO и проверка курсов валют")
    @ParameterizedTest(name = "{displayName}: {arguments}")
    @CsvSource({"https://www.yandex.ru, Открытие, Частным клиентам | Банк Открытие"})
    public void isOpeningContain(String url, String opening, String blockOpening) {
        chromeDriver.get(url);
        BeforeSearchOpening beforeSearchOpening = new BeforeSearchOpening(chromeDriver);
        beforeSearchOpening.find(opening);
        AfterSearchOpening afterSearchOpening = new AfterSearchOpening(chromeDriver);
        Assertions.assertTrue(
                afterSearchOpening.getResults().stream()
                        .anyMatch(webElement -> webElement.getText().contains(blockOpening)),
                "В поисковой выдаче по ключевому слову открытие нет блока со ссылкой '" + blockOpening + "'"
        );
        Assertions.assertTrue(
                afterSearchOpening.openLink(blockOpening),
                "Не удалось получить ссылку, либо переход по ссылке не произошел."
                );

        Assertions.assertTrue(
                afterSearchOpening.getExchangeRateElements(),
                "Не удалось найти хотя бы один элемент USD или EUR"
        );

        Assertions.assertTrue(
                afterSearchOpening.checkRates(),
                "Курс покупки выше курса продажи"
        );

    }

    @Feature("Проверка результатов поиска и содержания таблиц")
    @DisplayName("Проверка результатов поиска с помощью PF и проверка содержания таблицы")
    @ParameterizedTest(name = "{displayName}: {arguments}")
    @CsvSource({"https://www.yandex.ru, таблица, Сергей Владимирович, Сергей Адамович"})
    public void yandexTable(String searcherURL, String searchTable, String firstTeacher, String lastTeacher) {
        chromeDriver.get(searcherURL);
        PageFactoryTable pageFactoryTable = PageFactory.initElements(chromeDriver, PageFactoryTable.class);
        pageFactoryTable.find(searchTable);
        Assertions.assertTrue(
                pageFactoryTable.getResults().stream()
                        .anyMatch(next-> next.getText().contains("Таблица — Википедия")),
                "Результат поиска не содержит блок с заголовком 'Таблица - Википедия'"
        );
        Assertions.assertTrue(
                pageFactoryTable.clickAndSwitchToWikiPage(),
                "Нет ссылки на страницу, либо невозможет переход на страницу."
        );
        Assertions.assertTrue(
                pageFactoryTable.sureThatTableCaption(),
                "Нет таблицы с названием 'Преподаватели кафедры программирования'"
        );
        Assertions.assertTrue(
                pageFactoryTable.sureFirstTeacher(firstTeacher),
                firstTeacher + " в списке не первый."
        );
        Assertions.assertTrue(
                pageFactoryTable.sureLastTeacher(lastTeacher),
                lastTeacher + " не последний в списке."
        );
    }

}
